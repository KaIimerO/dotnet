﻿using ClientConvertisseurV1.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ClientConvertisseurV1.Service
{
    class WSService
    {
        private const string BASE_URL = "http://localhost:5000/api/";
        static HttpClient client = new HttpClient();

        private static WSService instance = new WSService();

        private WSService()
        {
            RunAsync();
        }

        public static WSService getInstance()
        {
            return instance;
        }

        static async Task RunAsync()
        {
            client.BaseAddress = new Uri(BASE_URL);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<List<Devise>> getAllDevisesAsync()
        {
            List<Devise> devises = new List<Devise>();
            HttpResponseMessage response = await client.GetAsync("devises");
            if (response.IsSuccessStatusCode)
            {
                devises = await response.Content.ReadAsAsync<List<Devise>>();
            }
            return devises;
        }
    }
}
