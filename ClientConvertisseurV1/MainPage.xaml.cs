﻿using ClientConvertisseurV1.Exceptions;
using ClientConvertisseurV1.Model;
using ClientConvertisseurV1.Service;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Pour plus d'informations sur le modèle d'élément Page vierge, consultez la page https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace ClientConvertisseurV1
{
    /// <summary>
    /// Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
            ActionGetData();
        }

        private async void ActionGetData()
        {
            try
            {
                var result = await WSService.getInstance().getAllDevisesAsync();
                this.comboBoxDevise.DataContext = new List<Devise>(result);
            } catch(Exception e)
            {
                ShowError(e);
            }
        }

        private async void ShowError(Exception exception)
        {
            new MessageDialog(exception.Message).ShowAsync() ;
        }

        private void buttonConvertir_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Double euros = Double.Parse(this.textBoxMontantEuro.Text.Replace(".",","));
                Devise devise = (Devise)this.comboBoxDevise.SelectedItem;
                if (devise == null)
                {
                    throw new DeviseNonValideException();
                }
                this.textBoxMontantDevise.Text = (euros * devise.Taux).ToString();
            } catch (FormatException exception)
            {
                ShowError(new MontantInvalideException());
            }
            catch (Exception exception)
            {
                ShowError(exception);
            }
            
        }
    }
}
