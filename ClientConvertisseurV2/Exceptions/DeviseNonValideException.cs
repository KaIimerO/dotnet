﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientConvertisseurV2.Exceptions
{
    class DeviseNonValideException : Exception
    {
        public DeviseNonValideException() : base("La devise est invalide.")
        {

        }
    }
}
