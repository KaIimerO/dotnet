﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientConvertisseurV2.Exceptions
{
    class MontantInvalideException : Exception
    {
        public MontantInvalideException() : base("Le montant est invalide.")
        {

        }
    }
}
