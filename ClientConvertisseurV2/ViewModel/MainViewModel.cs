﻿using ClientConvertisseurV2.Exceptions;
using ClientConvertisseurV2.Model;
using ClientConvertisseurV2.Service;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.UI.Popups;

namespace ClientConvertisseurV2.ViewModel
{
    public abstract class MainViewModel : ViewModelBase
    {
        private ObservableCollection<Devise> _comboBoxDevises;
        public ICommand BtnSetConversion { get; private set; }
        private string _montantEuros;
        private Devise _ComboBoxDeviseItem;
        private string _montantDevise;
        public string MontantEuros
        {
            get { return _montantEuros; }
            set
            {
                _montantEuros = value;
                RaisePropertyChanged();
            }
        }

        public Devise ComboBoxDeviseItem
        {
            get { return _ComboBoxDeviseItem; }
            set
            {
                _ComboBoxDeviseItem = value;
                RaisePropertyChanged();
            }
        }

        public string MontantDevise
        {
            get { return _montantDevise; }
            set
            {
                _montantDevise = value;
                RaisePropertyChanged();
            }
        }

        public ObservableCollection<Devise> ComboBoxDevises
        {
            get { return _comboBoxDevises; }
            set
            {
            _comboBoxDevises = value;
                RaisePropertyChanged();// Pour notifier de la modification de ses données
            }
        }

        public MainViewModel()
        {
            ActionGetData();
            BtnSetConversion = new RelayCommand(ActionSetConversion);
        }
        private async void ActionGetData()
        {
            try
            {
                var result = await WSService.getInstance().getAllDevisesAsync();
                this.ComboBoxDevises = new ObservableCollection<Devise>(result);
            } catch(Exception e)
            {
                ShowError(e);
            }
            
        }

        abstract protected void ActionSetConversion();

        protected async void ShowError(Exception exception)
        {
            new MessageDialog(exception.Message).ShowAsync();
        }

    }
}
