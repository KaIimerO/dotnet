﻿using ClientConvertisseurV2.Exceptions;
using ClientConvertisseurV2.Model;
using ClientConvertisseurV2.Service;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.UI.Popups;

namespace ClientConvertisseurV2.ViewModel
{
    public class SndPageViewModel : MainViewModel
    {
        protected override void ActionSetConversion()
        {
            try
            {
                Double montantDevise = Double.Parse(this.MontantDevise.Replace(".", ","));
                Devise devise = this.ComboBoxDeviseItem;
                if (devise == null)
                {
                    throw new DeviseNonValideException();
                }
                this.MontantEuros = (montantDevise / devise.Taux).ToString();
            } catch(FormatException fe)
            {
                ShowError(fe);
            } catch(Exception e)
            {
                ShowError(e);
            }
        }
    }
}
